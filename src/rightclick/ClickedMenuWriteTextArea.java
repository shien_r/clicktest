package rightclick;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ClickedMenuWriteTextArea extends JScrollPane {
	
	private static final long serialVersionUID = 1L;
	JTextArea jTextArea;
	
	ClickedMenuWriteTextArea() {
		jTextArea = new JTextArea();
		jTextArea.setColumns(20);
		setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		setViewportView(jTextArea);
	}
	
	public void insert (String message) {
		jTextArea.insert(message+"\n", 0);
	}
}
