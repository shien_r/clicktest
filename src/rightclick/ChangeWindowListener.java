package rightclick;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeWindowListener implements ActionListener {
	Component component;
	
	ChangeWindowListener (Component _component) {
		component = _component; 
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		changeWindowSize();
	}
	
	public void changeWindowSize() {
		Dimension dimension = component.getSize();
		dimension.height -= 10;
		dimension.width -= 10;
		component.setSize(dimension);
	}
}
