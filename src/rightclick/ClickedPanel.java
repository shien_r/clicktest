package rightclick;

import javax.swing.*;

import java.awt.Component;
import java.awt.event.*;
import java.util.ArrayList;

/* マウス操作クラス
 * 
 * 
 * MouseListener を implements するのではなく、
 * Mouse Adapter っていうのを extends すれば
 * 全部オーバーライドしなくてもいいっぽい 
 * */

public class ClickedPanel extends JPanel implements MouseListener{
	private static final long serialVersionUID = 1L;
	private static final String SMALL_WINDOW_SIZE = "スモール"; 
	JPopupMenu jPopupMenu = new JPopupMenu(); 
	ClickedMenuWriteTextArea clickedMenuWriteTextArea;
	
	ClickedPanel (ArrayList<String> arrayList, Component component, ClickedMenuWriteTextArea cmwta) {
		this.addMouseListener(this); // マウスリスナを登録
		int length = arrayList.size();
		clickedMenuWriteTextArea = cmwta;
		for (int i=0; i < length ;i++) {
			String name = arrayList.get(i);
			if (name.equals(SMALL_WINDOW_SIZE)) {
				addPopupMenuItem(name, new ChangeWindowListener(component));
			} else {
				addPopupMenuItem(name, new ClickedMenuListener(name, cmwta));
			}
		}
		
	}
	
	private JMenuItem addPopupMenuItem(String name, ActionListener actionListener) {
		JMenuItem item = new JMenuItem(name);
		item.addActionListener(actionListener);
		jPopupMenu.add(item);
		return item;
	}

	/* なんか時間開けたら e.getClickCount() が初期化されるみたい
	 * 操作する側はダブルクリックは長めに時間開けないといけない感じ
	 * */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			jPopupMenu.show(e.getComponent(), e.getX(), e.getY());
		}
		// キー指定のダブルクリック
		if (SwingUtilities.isLeftMouseButton(e)) {
			clickedMenuWriteTextArea.insert("左クリック");
			if (2 == e.getClickCount()) {
				clickedMenuWriteTextArea.insert("左ダブルクリック");
			}
		}
		/* どんなキーでもいいからダブルクリック以上
		if (2 == e.getClickCount()) {
			clickedMenuWriteTextArea.insert("ダブルクリック");
		} else if (3 == e.getClickCount()) {
			clickedMenuWriteTextArea.insert("トリプルクリック");
		} else if (3 <= e.getClickCount()) {
			clickedMenuWriteTextArea.insert("めっちゃクリック");
		}*/
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		// clickedMenuWriteTextArea.insert("押してます");
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		// clickedMenuWriteTextArea.insert("離しました");
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// マウスポインタが入ってきたら
		// clickedMenuWriteTextArea.insert("---ちーっす");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// マウスポインタが出て行ったら
		// clickedMenuWriteTextArea.insert("---おつかれっす");
		// TODO Auto-generated method stub
		
	}

	

}
