package rightclick;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;


public class RightClick extends JFrame{

	private static final long serialVersionUID = 1L;
	
	String groupA[] = { "あほ","ばか","Ruby", "スモール","C++", "Perl", "Java", "スモール"};
	String groupB[] = { "あほ","スモール", "ばか","Lisp", "Haskell", "Scheme", "CbC", "スモール"};
	
	RightClick() {
		super();
		setLayout(new GridLayout(2,1));
		setSize(500,700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		JPanel upperPanel = new JPanel();
		upperPanel.setLayout(new GridLayout(1,2));
		ClickedMenuWriteTextArea cmwta = new ClickedMenuWriteTextArea();
		
		configurePanel(cmwta, groupA,Color.GREEN, upperPanel);
		configurePanel(cmwta, groupB, Color.ORANGE, upperPanel);
		
		add(upperPanel);
		add(cmwta);
	}
	
	private void configurePanel(ClickedMenuWriteTextArea cmwta, String group[], 
			Color bgcolor, JPanel upperPanel) {
		
		ArrayList<String> arrayList = new ArrayList<String>();
		
		for (int i=0; i < group.length; i++) {
			arrayList.add(group[i]);
		}
		
		ClickedPanel panel = new ClickedPanel(arrayList, this, cmwta);
		panel.setBackground(bgcolor);
		upperPanel.add(panel);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new RightClick();

	}

}
