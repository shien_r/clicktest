package rightclick;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClickedMenuListener implements ActionListener {
	private ClickedMenuWriteTextArea clickedMenuWriteTextArea;
	private String manuName;
	
	ClickedMenuListener (String name, ClickedMenuWriteTextArea _cmwta) {
		manuName = name;
		clickedMenuWriteTextArea = _cmwta;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		clickedMenuWriteTextArea.insert(manuName);
	}

}
